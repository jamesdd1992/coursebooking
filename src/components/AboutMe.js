import {Row, Col} from 'react-bootstrap';



export default function AboutMe() {
    return(

         <Row className='m-5'>
        <Col>
        
        <h1 className='bg-primary btn btn-primary'>About Me</h1>
        <h2 className="authorName mt-4" >James David</h2>
        <h2 className='mb-3 mt-3'>Full Stack Web Developer</h2>
            <p>I am from Mabalacat city Pampanga. I am a career shifter and currently taking a web development course at zuitt.com. I had learned a lot of knowledge in web development these past few days. I hope I will continue to learn more.</p>
            <h2 className='mt-5'>Contact Me</h2>
            <ul>
                <li >Email:jamesdd1992@gmail.com</li>
                <li>Mobile No:09975110865</li>
                <li>Address:Mabalacat City, Pampanga.</li>
            </ul>
            
        </Col>
      
        <Col>
  
        </Col>
        </Row>
           
       
      
       
 
    );
};

