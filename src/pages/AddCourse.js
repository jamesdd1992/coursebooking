//identify the components that will used for this page

import {Form, Button, Container} from 'react-bootstrap'
import swal from 'sweetalert2'

export default function addCourse(){
   
    const addCourse = (eventSubmit) => {

        eventSubmit.preventDefault()
        return(
            document.getElementById('formAddCourse').reset(),
                swal.fire({
                icon: 'success',
                title: 'Course Successfully Added!',
                text: 'Please wait while redirecting.',
              }) 
           
        );
    };
    return(
        <div>
               
            <Container>
            <h1 className='text-center bg-primary mt-2 p-3 border'>Add Course</h1>
                 <Form onSubmit={e => addCourse(e)} id="formAddCourse">
                     
                     {/* Email Address Field */}
                     <Form.Group>
                         <Form.Label>Course</Form.Label>
                         <Form.Control type='text' required placeholder='Enter course Name'></Form.Control>
                     </Form.Group>
                     {/* Password Field */}
                     <Form.Group>
                         <Form.Label>Description</Form.Label>
                         <Form.Control type='text' required placeholder='Enter Description'></Form.Control>
                     </Form.Group>
                     <Form.Group>
                         <Form.Label>Price</Form.Label>
                         <Form.Control type='number' required placeholder='Enter price'></Form.Control>
                     </Form.Group>
                     <Button className='btn btn-block bg-primary' type='submit'>Submit</Button>
                     
                 </Form>
            </Container>
            
        </div>
        
    );
}